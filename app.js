
var myApp = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngMaterial', 'vAccordion'])
    .config(function($routeProvider, $locationProvider) {
        
        $routeProvider
            .when('/',
            {
                templateUrl: 'views/main.html',
                controller: 'MainController'
            })
            .when('/network',
            {
                templateUrl: 'views/network.html',
                controller: 'NetworkController'
            })
            .when('/security',
            {
                templateUrl: 'views/security.html',
                controller: 'SecurityController'
            })
            .when('/ntp',
            {
                templateUrl: 'views/ntp.html',
                controller: 'NtpController'
            })
            .otherwise({
                    redirectTo: '/'
            });
            
});