myApp.controller('NetworkController', 
    function NetworkController($scope, $mdDialog) {
        
        $scope.ipv4 = {
            ip: '172.16.100.100'
        };
      
        $scope.showAlert = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Warning')
                    .textContent('You can not add an item.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Ok')
                    .targetEvent(ev)
            );
        };
    }
)

