myApp.controller('NtpController', 
    function NtpController($scope) {
        
        $scope.stratum = [1, 2, 3, 4, 5, 6, 7 ,8, 9, 10];
        $scope.trusttime = 0;
        $scope.stratum_def = 'Days';
        $scope.period = [ 'Days', 'Hours', 'Minutes', 'Seconds' ];
    }
)

